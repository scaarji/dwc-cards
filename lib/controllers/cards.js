'use strict';
var async = require('async'),
    _ = require('lodash');

var models = require('../models');
var errors = require('../errors');

var Card = models.Card;

var CardsController = function CardsController(repos) {
    this.repos = repos;
};

CardsController.prototype.createCard = function createCard(data, callback) {
    var _this = this;
    var card = new Card(data);
    var validationResult = card.isValid();

    if (!validationResult.valid) {
        return callback(new errors.BadArgument(validationResult.message));
    }

    var findDeck = function findDeck(callback) {
            _this.repos.deck.findById(data.deck, callback);
        },
        storeCard = function storeCard(deck, callback) {
            if (!deck) {
                return callback(new errors.NotFound('Deck not found'));
            }

            _this.repos.card.store(card, callback);
        };

    async.waterfall([
        findDeck,
        storeCard
    ], callback);
};

CardsController.prototype.findCards = function findCards(filters, callback) {
    var _this = this,
        findDeck = function findDeck(callback) {
            _this.repos.deck.findById(filters.deck, callback);
        },
        findCardsByDeck = function findCardsByDeck(deck, callback) {
            if (!deck) {
                return callback(new errors.NotFound('Deck not found'));
            }

            _this.repos.card.findByDeck(deck.id, {since: filters.since}, callback);
        };

    if (!filters.deck) {
        return process.nextTick(function() {
            callback(new errors.BadArgument('deck is required'));
        });
    }

    async.waterfall([
        findDeck,
        findCardsByDeck
    ], callback);
};

CardsController.prototype.findCard = function findCard(params, callback) {
    if (!params.id) {
        return process.nextTick(function() {
            callback(new errors.BadArgument('id is required'));
        });
    }

    this.repos.card.findById(params.id, callback);
};

CardsController.prototype.updateCard = function updateCard(params, callback) {
    if (!params.id) {
        return process.nextTick(function() {
            callback(new errors.BadArgument('id is required'));
        });
    }

    var _this = this,
        findCard = function findCard(callback) {
            _this.repos.card.findById(params.id, callback);
        },
        storeUpdatedCard = function storeUpdatedCard(card, callback) {
            var data = _.pick(params, [
                'front', 'back',
                'updatedAt', 'showAt',
                'status'
            ]);
            if (!data.updatedAt) {
                data.updatedAt = Date.now();
            }

            _.each(data, function(value, key) {
                card[key] = value;
            });

            var validationResult = card.isValid();

            if (!validationResult.valid) {
                return callback(new errors.BadArgument(validationResult.message));
            }

            _this.repos.card.store(card, callback);
        };

    async.waterfall([
        findCard,
        storeUpdatedCard
    ], callback);
};

module.exports = CardsController;
