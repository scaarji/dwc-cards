'use strict';
module.exports = {
    Decks: require('./decks'),
    Cards: require('./cards')
};
