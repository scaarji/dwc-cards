'use strict';
var models = require('../models');
var errors = require('../errors');

var Deck = models.Deck;

var DeckController = function DeckController(repos) {
    this.repos = repos;
};

DeckController.prototype.createDeck = function createDeck(data, callback) {
    var deck = new Deck(data);
    var validationResult = deck.isValid();
    if (!validationResult.valid) {
        return callback(new errors.BadArgument(validationResult.message));
    }

    this.repos.deck.store(deck, callback);
};

DeckController.prototype.findDecks = function findDecks(data, callback) {
    if (!data.user) {
        return process.nextTick(function() {
            callback(new errors.BadArgument('user is required'));
        });
    }

    this.repos.deck.findByUser(data.user, callback);
};

DeckController.prototype.findDeck = function findDeck(data, callback) {
    if (!data.id) {
        return process.nextTick(function() {
            callback(new errors.BadArgument('id is required'));
        });
    }

    this.repos.deck.findById(data.id, callback);
};

module.exports = DeckController;
