'use strict';
var model = require('dwc-model');

var NAME_UNDEFINED = 'noname';

var schema = {
    optional: false,
    type: 'object',
    properties: {
        id: {
            type: 'string',
            optional: false,
        },
        name: {
            type: 'string',
            optional: false,
            def: NAME_UNDEFINED
        },
        user: {
            type: 'string',
            optional: false
        }
    }
};

var Deck = model('Deck', schema);

module.exports = Deck;

