'use strict';
var model = require('dwc-model');

var schema = {
    optional: false,
    type: 'object',
    properties: {
        id: {
            type: 'string',
            optional: false
        },
        deck: {
            type: 'string',
            optional: false
        },
        status: {
            type: 'string',
            def: 'active',
            $enum: ['active', 'inactive'],
            optional: false
        },
        front: {
            type: 'string',
            optional: false
        },
        back: {
            type: 'string',
            optional: false
        },
        createdAt: {
            type: 'number',
            def: Date,
            optional: false
        },
        updatedAt: {
            type: 'number',
            def: Date,
            optional: false
        },
        showAt: {
            type: 'number',
            def: 0,
            optional: false
        }
    }
};
var defaults = {
    createdAt: Date.now,
    updatedAt: Date.now,
    showAt: 0
};

var Card = model('Card', schema, defaults);

module.exports = Card;
