'use strict';
var _ = require('lodash');

var errorNames = ['NotFound', 'BadArgument'];
var errors = _.reduce(errorNames, function(errors, name) {
    errors[name] = function(message) {
        Error.call(this);
        this.name = name;
        this.message = message;
    };

    errors[name].prototype = new Error();

    return errors;
}, {});

module.exports = errors;
