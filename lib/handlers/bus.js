'use strict';
var _ = require('lodash');

var processRequest = {
    deck: function(req, callback) {
        return function(err, deck) {
            if (err) {
                callback(err);
            } else {
                callback(null, deck.toObject());
            }
        };
    },
    card: function(req, callback) {
        return function(err, card) {
            if (err) {
                callback(err);
            } else {
                callback(null, card.toObject());
            }
        };
    },
    decks: function(req, callback) {
        return function(err, decks) {
            if (err) {
                callback(err);
            } else {
                callback(null, _.map(decks, function(deck) {
                    return deck.toObject();
                }));
            }
        };
    },
    cards: function(req, callback) {
        return function(err, cards) {
            if (err) {
                callback(err);
            } else {
                callback(null, _.map(cards, function(card) {
                    return card.toObject();
                }));
            }
        };
    }
};

module.exports = function(controllers) {
    return {
        createDeck: function(req, callback) {
            controllers.decks.createDeck(req.params, processRequest.deck(req, callback));
        },
        findDecks: function(req, callback) {
            controllers.decks.findDecks(req.params, processRequest.decks(req, callback));
        },
        findDeck: function(req, callback) {
            controllers.decks.findDeck(req.params, processRequest.deck(req, callback));
        },
        createCard: function(req, callback) {
            controllers.cards.createCard(req.params, processRequest.card(req, callback));
        },
        findCards: function(req, callback) {
            controllers.cards.findCards(req.params, processRequest.cards(req, callback));
        },
        findCard: function(req, callback) {
            controllers.cards.findCard(req.params, processRequest.card(req, callback));
        },
        updateCard: function(req, callback) {
            controllers.cards.updateCard(req.params, processRequest.card(req, callback));
        }
    };
};
