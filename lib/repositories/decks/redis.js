'use strict';
var _ = require('lodash'),
    async = require('async');

var Deck = require('../../models').Deck;
var redisStore = require('../default').redis;

var DeckStore = redisStore(Deck);

DeckStore.prototype.getUserKey = function getUserKey(user) {
    return [this.getPrefix(), Deck.name, 'byuser', user].join(':');
};

DeckStore.prototype.store = function store(deck, callback) {
    if (!deck instanceof Deck) {
        return process.nextTick(function() {
            callback(new TypeError('item must be instance of ' + Deck.name));
        });
    }

    this.redisClient.multi()
        .hmset(this.getKey(deck.id), deck.toObject())
        .sadd(this.getUserKey(deck.user), deck.id)
        .exec(function(err) {
            if (err) {
                callback(err);
            } else {
                callback(null, deck);
            }
        });
};

DeckStore.prototype.findByUser = function findByUser(user, callback) {
    var _this = this,
        findIds = function findIds(callback) {
            _this.redisClient.smembers(_this.getUserKey(user), callback);
        },
        findItems = function findItems(ids, callback) {
            if (!ids || !ids.length) {
                return callback(null, []);
            }

            _.reduce(ids, function(multi, id) {
                return multi.hgetall(_this.getKey(id));
            }, _this.redisClient.multi()).exec(callback);
        },
        formatResponse = function formatResponse(items, callback) {
            if (!items || !items.length) {
                return callback(null, []);
            }

            var decks = _(items)
                .compact()
                .map(function(item) {
                    return new Deck(item);
                })
                .value();

            callback(null, decks);
        };

    async.waterfall([
        findIds,
        findItems,
        formatResponse
    ], callback);
};

module.exports = DeckStore;
