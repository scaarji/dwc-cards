'use strict';
var _ = require('lodash'),
    async = require('async');

var errors = require('../../errors');

var PREFIX = 'DWC';
module.exports = function(Model) {
    var Store = function Store(redisClient) {
        this.redisClient = redisClient;
    };

    Store.prototype.getPrefix = function getPrefix() {
        return PREFIX;
    };

    Store.prototype.getKey = function getKey(id) {
        return [PREFIX, Model.modelName, id].join(':');
    };

    Store.prototype.store = function store(obj, callback) {
        if (!obj instanceof Model) {
            return process.nextTick(function() {
                callback(new TypeError('item must be instance of ' + Model.modelName));
            });
        }

        this.redisClient.hmset(this.getKey(obj.id), obj.toObject(), function(err) {
            if (err) {
                callback(err);
            } else {
                callback(null, obj);
            }
        });
    };

    Store.prototype.findById = function findById(id, callback) {
        if (!id) {
            return process.nextTick(function() {
                callback(new errors.BadArgument('id is required'));
            });
        }

        var _this = this,
            findObject = function findObject(callback) {
                _this.redisClient.hgetall(_this.getKey(id), callback);
            },
            formatResponse = function formatResponse(data, callback) {
                if (!data) {
                    return callback(new errors.NotFound(Model.modelName + ' not found'));
                }

                callback(null, new Model(data));
            };

        async.waterfall([
            findObject,
            formatResponse
        ], callback);
    };

    Store.prototype.truncate = function truncate(callback) {
        var _this = this,
            findItems = function findItems(callback) {
                _this.redisClient.keys(_this.getKey('*'), callback);
            },
            removeItems = function removeItems(keys, callback) {
                if (!keys || !keys.length) {
                    return callback(null);
                }

                _.reduce(keys, function(multi, key) {
                    return multi.del(key);
                }, _this.redisClient.multi()).exec(function(err) {
                    callback(err);
                });
            };

        async.waterfall([
            findItems,
            removeItems
        ], callback);
    };

    return Store;
};
