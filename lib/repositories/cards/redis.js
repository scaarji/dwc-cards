'use strict';
var _ = require('lodash'),
    async = require('async');

var errors = require('../../errors');

var Card = require('../../models').Card;
var redisStore = require('../default').redis;

var CardStore = redisStore(Card);

CardStore.prototype.getDeckKey = function getDeckKey(deck) {
    return [this.getPrefix(), Card.name, 'bydeck', deck].join(':');
};

CardStore.prototype.store = function store(card, callback) {
    if (!card instanceof Card) {
        return process.nextTick(function() {
            callback(new TypeError('item must be instance of ' + Card.name));
        });
    }

    this.redisClient.multi()
        .hmset(this.getKey(card.id), card.toObject())
        .zadd(this.getDeckKey(card.deck), card.updatedAt, card.id)
        .exec(function(err) {
            if (err) {
                callback(err);
            } else {
                callback(null, card);
            }
        });
};

CardStore.prototype.findByDeck = function findByDeck(deck, filters, callback) {
    if (typeof filters === 'function') {
        callback = filters;
        filters = {};
    }

    if (!deck) {
        return process.nextTick(function() {
            callback(new errors.BadArgument('deck is required'));
        });
    }

    var _this = this,
        findIds = function findIds(callback) {
            var since = filters.since || 0;
            _this.redisClient.zrangebyscore(_this.getDeckKey(deck), since, '+inf', callback);
        },
        findItems = function findItems(ids, callback) {
            if (!ids || !ids.length) {
                return callback(null, []);
            }

            _.reduce(ids, function(multi, id) {
                return multi.hgetall(_this.getKey(id));
            }, _this.redisClient.multi()).exec(callback);
        },
        formatResponse = function formatResponse(items, callback) {
            if (!items || !items.length) {
                return callback(null, []);
            }

            var cards = _(items)
                .compact()
                .map(function(item) {
                    return new Card(item);
                })
                .value();

            callback(null, cards);
        };

    async.waterfall([
        findIds,
        findItems,
        formatResponse
    ], callback);
};

module.exports = CardStore;
