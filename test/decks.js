/* global describe, it, afterEach */
'use strict';
var async = require('async'),
    should = require('should'),
    _ = require('lodash');

module.exports = function(common) {
    describe('Decks API', function() {
        describe('#createDeck', function() {
            afterEach(common.truncate);

            it('should return error on empty user', function(done) {
                common.query({
                    op: 'createDeck',
                    params: {
                        name: 'deck'
                    }
                }, function(err, result) {
                    should.exist(err);
                    err.message.should.match(/user/i);
                    should.not.exist(result);

                    done();
                });
            });

            it('should create and return deck', function(done) {
                var deckInfo = {
                    user: 'user',
                    name: 'deck'
                };
                common.query({
                    op: 'createDeck',
                    params: deckInfo
                }, function(err, deck) {
                    should.not.exist(err);
                    should.exist(deck);
                    deck.should.have.properties(['id', 'name', 'user']);
                    deck.should.have.properties(deckInfo);

                    done();
                });
            });
        });

        describe('#findDeck', function() {
            afterEach(common.truncate);

            it('should return error on empty id', function(done) {
                common.query({
                    op: 'findDeck',
                    params: {}
                }, function(err, result) {
                    should.exist(err);
                    err.message.should.match(/id/i);
                    should.not.exist(result);

                    done();
                });
            });

            it('should return NotFound error on nonexistent id', function(done) {
                common.query({
                    op: 'findDeck',
                    params: {
                        id: 'non-existent'
                    }
                }, function(err, result) {
                    should.exist(err);
                    err.message.should.match(/not found/i);
                    should.not.exist(result);

                    done();
                });

            });

            it('should return deck', function(done) {
                async.waterfall([
                    function createDeck(callback) {
                        common.query({
                            op: 'createDeck',
                            params: {
                                user: 'user',
                                name: 'deck'
                            }
                        }, callback);
                    },
                    function findDeck(deck, callback) {
                        common.query({
                            op: 'findDeck',
                            params: {
                                id: deck.id
                            }
                        }, function(err, result) {
                            should.not.exist(err);
                            should.exist(result);
                            result.should.eql(deck);

                            callback();
                        });
                    }
                ], done);
            });
        });

        describe('#findDecks', function() {
            afterEach(common.truncate);

            it('should return error on empty user', function(done) {
                common.query({
                    op: 'findDecks',
                    params: {}
                }, function(err, result) {
                    should.exist(err);
                    err.message.should.match(/user/i);
                    should.not.exist(result);

                    done();
                });
            });

            it('should return decks by user', function(done) {
                var user = 'user';
                var fixtures = [
                    {
                        user: user,
                        name: 'deck1'
                    },
                    {
                        user: user,
                        name: 'deck2'
                    },
                    {
                        user: 'other-user',
                        name: 'deck1'
                    },
                    {
                        user: user,
                        name: 'deck3'
                    },
                    {
                        user: 'yet-another-user',
                        name: 'deck4'
                    }
                ];

                async.waterfall([
                    function createDecks(callback) {
                        async.each(fixtures, function createDeck(fixture, callback) {
                            common.query({
                                op: 'createDeck',
                                params: fixture
                            }, callback);
                        }, callback);
                    },
                    function findDecks(callback) {
                        common.query({
                            op: 'findDecks',
                            params: {
                                user: user
                            }
                        }, function(err, result) {
                            should.not.exist(err);
                            should.exist(result);
                            result.should.be.an.Array.and.have.length(3);
                            _.each(result, function(deck) {
                                deck.should.have.property('user', user);
                            });

                            callback();
                        });
                    }
                ], done);
            });
        });
    });
};

