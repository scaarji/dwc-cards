/* global describe, before */
'use strict';
var async = require('async'),
    redis = require('redis'),
    _ = require('lodash');

var startService = require('../index');

var config = require('./config.json');

var DecksRepo = require('../lib/repositories/decks').Redis,
    CardsRepo = require('../lib/repositories/cards').Redis;

describe('Cards Service', function() {
    var service,
        client,
        info;

    var redisClient = redis.createClient(config.redis.port, config.redis.host);
    var repos = {
        decks: new DecksRepo(redisClient),
        cards: new CardsRepo(redisClient)
    };

    before(function(done) {
        service = startService(config, done);
        client = service.client;
        info = service.info();
    });

    var truncate = function(done) {
        async.parallel([
            repos.decks.truncate.bind(repos.decks),
            repos.cards.truncate.bind(repos.cards)
        ], done);
    };

    var common = {
        truncate: truncate,
        query: function(msg, callback) {
            client.query(_.extend({service: info}, msg), callback);
        }
    };

    require('./decks')(common);
    require('./cards')(common);
});
