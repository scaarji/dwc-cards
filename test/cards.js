/* global describe, it, before, after, afterEach, Map */
'use strict';
var async = require('async'),
    should = require('should'),
    _ = require('lodash');

module.exports = function(common) {
    describe('Cards API', function() {
        describe('#createCard', function() {
            afterEach(common.truncate);

            it('should return error on empty deck', function(done) {
                common.query({
                    op: 'createCard',
                    params: {
                        name: 'deck',
                        front: 'front',
                        back: 'back'
                    }
                }, function(err, result) {
                    should.exist(err);
                    err.message.should.match(/deck/i);
                    should.not.exist(result);

                    done();
                });
            });

            it('should return error on nonexistent deck', function(done) {
                common.query({
                    op: 'createCard',
                    params: {
                        deck: 'deck',
                        front: 'front',
                        back: 'back'
                    }
                }, function(err, result) {
                    should.exist(err);
                    err.name.should.eql('NotFound');
                    err.message.should.match(/deck/i);
                    should.not.exist(result);

                    done();
                });
            });

            it('should create and return card', function(done) {
                var cardInfo = {
                    front: 'front',
                    back: 'back'
                };

                async.waterfall([
                    function createDeck(callback) {
                        common.query({
                            op: 'createDeck',
                            params: {
                                user: 'user',
                                name: 'name'
                            }
                        }, callback);
                    },
                    function createCard(deck, callback) {
                        cardInfo.deck = deck.id;
                        common.query({
                            op: 'createCard',
                            params: cardInfo
                        }, callback);
                    },
                    function checkResult(card, callback) {
                        card.should.have.properties(['id', 'deck', 'front', 'back',
                                                    'createdAt', 'updatedAt', 'showAt']);
                        card.should.have.properties(cardInfo);
                        card.status.should.eql('active');

                        callback();
                    }
                ], done);
            });
        });

        describe('#findCards', function() {
            after(common.truncate);

            var now = Date.now();
            var decks, cardsCnt;

            before(function(done) {
                async.waterfall([
                    function createDecks(callback) {
                        var fixtures = [
                            {
                                user: 'user',
                                name: 'deck1'
                            },
                            {
                                user: 'user',
                                name: 'deck2'
                            },
                            {
                                user: 'other-user',
                                name: 'deck'
                            }
                        ];

                        async.map(fixtures, function createDeck(fixture, callback) {
                            common.query({
                                op: 'createDeck',
                                params: fixture
                            }, callback);
                        }, callback);
                    },
                    function createCards(createdDecks, callback) {
                        decks = createdDecks;

                        cardsCnt = new Map([[decks[0].id, 5], [decks[1].id, 3], [decks[2].id, 2]]);

                        async.each(decks, function(deck, callback) {
                            async.times(cardsCnt.get(deck.id), function(i, callback) {
                                common.query({
                                    op: 'createCard',
                                    params: {
                                        deck: deck.id,
                                        front: 'front of ' + i,
                                        back: 'back of ' + i,
                                        createdAt: (now - i * 1000),
                                        updatedAt: (now - i * 1000)
                                    }
                                }, callback);
                            }, callback);
                        }, callback);
                    }
                ], done);
            });

            it('should return error on missing deck argument', function(done) {
                common.query({
                    op: 'findCards',
                    params: {
                    }
                }, function(err) {
                    should.exist(err);
                    err.name.should.eql('BadArgument');

                    done();
                });
            });
            it('should return error on nonexistent deck', function(done) {
                common.query({
                    op: 'findCards',
                    params: {
                        deck: 'no-such-deck'
                    }
                }, function(err) {
                    should.exist(err);
                    err.name.should.eql('NotFound');

                    done();
                });
            });
            it('should return cards from deck (existent deck)', function(done) {
                async.each(decks, function(deck, callback) {
                    async.waterfall([
                        function findCards(callback) {
                            common.query({
                                op: 'findCards',
                                params: {
                                    deck: deck.id
                                }
                            }, callback);
                        },
                        function verifyResult(cards, callback) {
                            cards.should.be.an.Array.and.have.length(cardsCnt.get(deck.id));

                            cards.forEach(function(card) {
                                card.should.have.properties([
                                    'id', 'deck', 'front', 'back',
                                    'createdAt', 'updatedAt', 'showAt'
                                ]);
                                card.deck.should.eql(deck.id);
                            });

                            callback();
                        }
                    ], callback);
                }, done);
            });
            it('should return cards from deck (existent deck, since filter)', function(done) {
                var since = now - 500;
                async.each(decks, function(deck, callback) {
                    async.waterfall([
                        function findCards(callback) {
                            common.query({
                                op: 'findCards',
                                params: {
                                    deck: deck.id,
                                    since: since
                                }
                            }, callback);
                        },
                        function verifyResult(cards, callback) {
                            cards.forEach(function(card) {
                                card.should.have.properties([
                                    'id', 'deck', 'front', 'back',
                                    'createdAt', 'updatedAt', 'showAt'
                                ]);
                                card.deck.should.eql(deck.id);
                                card.updatedAt.should.not.be.below(since);
                            });

                            callback();
                        }
                    ], callback);
                }, done);
            });
        });

        describe('#findCard', function() {
            var card;
            before(function(done) {
                async.waterfall([
                    function createDeck(callback) {
                        common.query({
                            op: 'createDeck',
                            params: {
                                user: 'user',
                                name: 'deck'
                            }
                        }, callback);
                    },
                    function createCard(deck, callback) {
                        common.query({
                            op: 'createCard',
                            params: {
                                deck: deck.id,
                                front: 'front',
                                back: 'back'
                            }
                        }, callback);
                    },
                    function storeCard(createdCard, callback) {
                        card = createdCard;

                        callback();
                    }
                ], done);
            });
            after(common.truncate);

            it('should return BadArgument error on missing id', function(done) {
                common.query({
                    op: 'findCard',
                    params: {}
                }, function(err) {
                    should.exist(err);
                    err.name.should.eql('BadArgument');

                    done();
                });
            });

            it('should return NotFound error on nonexistent card', function(done) {
                common.query({
                    op: 'findCard',
                    params: {
                        id: 'non-existent-id'
                    }
                }, function(err) {
                    should.exist(err);
                    err.name.should.eql('NotFound');

                    done();
                });
            });

            it('should return card object on existent card', function(done) {
                common.query({
                    op: 'findCard',
                    params: {
                        id: card.id
                    }
                }, function(err, returnedCard) {
                    should.not.exist(err);
                    returnedCard.should.eql(card);

                    done();
                });
            });
        });

        describe('#updateCard', function() {
            var card;

            before(function(done) {
                async.waterfall([
                    function createDeck(callback) {
                        common.query({
                            op: 'createDeck',
                            params: {
                                user: 'user',
                                name: 'deck'
                            }
                        }, callback);
                    },
                    function createCard(deck, callback) {
                        common.query({
                            op: 'createCard',
                            params: {
                                deck: deck.id,
                                front: 'front',
                                back: 'back'
                            }
                        }, callback);
                    },
                    function storeCard(createdCard, callback) {
                        card = createdCard;

                        callback();
                    }
                ], done);
            });
            after(common.truncate);

            it('should return BadArgument error on missing id', function(done) {
                common.query({
                    op: 'updateCard',
                    params: {
                        front: 'new front',
                        back: 'new back'
                    }
                }, function(err) {
                    should.exist(err);
                    err.name.should.eql('BadArgument');

                    done();
                });
            });

            it('should return NotFound error on nonexistent card', function(done) {
                common.query({
                    op: 'updateCard',
                    params: {
                        id: 'non-existent-id',
                        front: 'new front',
                        back: 'new back'
                    }
                }, function(err) {
                    should.exist(err);
                    err.name.should.eql('NotFound');

                    done();
                });
            });

            it('should return BadArgument error on invalid data', function(done) {
                common.query({
                    op: 'updateCard',
                    params: {
                        id: card.id,
                        front: 123,
                        back: 312,
                        status: 'invalid',
                        showAt: 'asdfasdf',
                        updatedAt: 123123
                    }
                }, function(err) {
                    should.exist(err);
                    err.name.should.eql('BadArgument');
                    console.log(err);

                    done();
                });
            });

            it('should return updated card object on existent card', function(done) {
                var fixture = {
                    front: 'new front',
                    back: 'new back',
                    deck: 'other-deck',
                    createdAt: Date.now() + 100,
                    updatedAt: Date.now() + 1000,
                    showAt: Date.now() + 10000,
                    status: 'inactive'
                };

                common.query({
                    op: 'updateCard',
                    params: _.extend({id: card.id}, fixture)
                }, function(err, updatedCard) {
                    should.not.exist(err);

                    updatedCard.should.have.properties({
                        id: card.id,
                        deck: card.deck,
                        front: fixture.front,
                        back: fixture.back,
                        updatedAt: fixture.updatedAt,
                        createdAt: card.createdAt,
                        showAt: fixture.showAt,
                        status: fixture.status
                    });

                    done();
                });
            });
        });
    });
};

