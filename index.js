#!/usr/bin/env node
'use strict';
var fs = require('fs');

var optimist = require('optimist');
var redis = require('redis');

var Client = require('dwc-client');
var Service = require('dwc-service');
var loggerMiddleware = require('dwc-middleware-logger');

var DecksRepo = require('./lib/repositories/decks').Redis;
var CardsRepo = require('./lib/repositories/cards').Redis;

var controllers = require('./lib/controllers');
var createHandlers = require('./lib/handlers/bus');

var startService = module.exports = function(config, callback) {
    // redis
    var redisClient = redis.createClient(config.redis.port, config.redis.host);
    // repos
    var deckRepo = new DecksRepo(redisClient);
    var cardRepo = new CardsRepo(redisClient);
    // controllers
    var decksController = new controllers.Decks({
        deck: deckRepo,
        card: cardRepo
    });
    var cardsController = new controllers.Cards({
        deck: deckRepo,
        card: cardRepo
    });
    // handlers
    var handlers = createHandlers({
        decks: decksController,
        cards: cardsController
    });
    // client
    var client = new Client(config);
    // service
    var service = new Service(client, {name: 'cards', version: 1}, handlers);
    // register
    service.register(loggerMiddleware({info: console.log}));
    // start
    service.start(callback);
    // return
    return service;
};

if (!module.parent) {
    var argv = optimist
        .demand('c')
        .alias('c', 'config')
        .describe('c', 'Path to configuration file')
        .argv;

    if (!fs.existsSync(argv.c)) {
        throw new Error('Config [' + argv.c + '] not found');
    }

    var contents = fs.readFileSync(argv.c, {encoding: 'utf8'}),
        config;

    try {
        config = JSON.parse(contents);
    } catch (err) {
        throw new Error('Failed to parse config json, ' + err.message);
    }

    startService(config);
}
